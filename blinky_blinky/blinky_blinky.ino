void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  delay(100);
  pinMode(D5, OUTPUT);
  Serial.println("ready!");
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(D5, HIGH);
  delay(1000);
  digitalWrite(D5,LOW);
  delay(1000);
}
