#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

const char* ssid = "*";
const char* password = "*";

ESP8266WebServer server(80);

bool zugangGestattet = false;
long duration, distance;

void handleRoot() {
  server.send(200, "text/plain", "hallo welt! hier ist ein ESP9266");
}

void handleNotFound(){
  server.send(404, "text/plain", "hier gibt es nichts zu sehen!");
}

void zugangGestattet() {
  zugangGestattet = true;
  Serial.println("on");
  server.send(200, "text/plain","eltern duerfen reinkommen");
}

void zugangNichtGestattet() {
  zugangGestattet = false;
  Serial.println("off");
  server.send(200, "text/plain","eltern duerfen nicht reinkommen");
}

void setup(void){
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Verbinden mit WLAN ");
  Serial.println(ssid);
  Serial.print("IP Adresse: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS gestartet");
  }

  server.on("/", handleRoot);
  server.on("/on", zugangGestattet);
  server.on("/off", zugangNichtGestattet);
  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("Webserver gestartet");
}

void loop(void){
  server.handleClient();
  if(zugangGestattet == true) {
    // led grün einstellen, led rot ausschalten
  } else {
    // led rot einstellen, led grün ausschalten
  }
 
}

void stehtJemandVorDerTuer() {
  digitalWrite(trigger, LOW);
  delayMicroseconds(2);
  digitalWrite(trigger, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigger, LOW);
  duration = pulseIn(echo, HIGH);
  //Calculate the distance (in cm) based on the speed of sound.
  distance = duration/58.2;
  Serial.println(distance);
  if(distance < 30) {
    // led einstellen
  } else {
    // led ausschalten
  }
  delay(50); 
}
