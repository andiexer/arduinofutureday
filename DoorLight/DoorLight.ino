#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

const char* ssid = "*";
const char* password = "*";

ESP8266WebServer server(80);

const int led = 14;
const int ledDoor = 16;
const int trigger = 12;
const int echo = 13;
bool ledOn = false;
long duration, distance;

void handleRoot() {
  server.send(200, "text/plain", "hello world! my first webserver");
}

void handleNotFound(){
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void handleLightOn() {
  ledOn = true;
  Serial.println("on");
  server.send(200, "text/plain","access granted");
}

void handleLightOff() {
  ledOn = false;
  Serial.println("off");
  server.send(200, "text/plain","access denied");
}

void setup(void){
  pinMode(led, OUTPUT);
  pinMode(trigger, OUTPUT);
  pinMode(echo, INPUT);
  pinMode(ledDoor, OUTPUT);
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);

  server.on("/on", handleLightOn);
  server.on("/off", handleLightOff);
  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void){
  server.handleClient();
  if(ledOn == true) {
    digitalWrite(ledDoor , HIGH);
  } else {
    digitalWrite(ledDoor, LOW);
  }
  digitalWrite(trigger, LOW);
  delayMicroseconds(2);
  digitalWrite(trigger, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigger, LOW);
  duration = pulseIn(echo, HIGH);
  //Calculate the distance (in cm) based on the speed of sound.
  distance = duration/58.2;
  Serial.println(distance);
  if(distance < 30) {
    digitalWrite(led, HIGH);
  } else {
    digitalWrite(led, LOW);
  }
  //Delay 50ms before next reading.
  delay(50);  
}
